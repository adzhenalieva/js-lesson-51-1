import React from 'react';

const Header = (props) => {
    return (
        <div className="header">
            <a className="logo" href="">{props.title}</a>
            <nav>
                <a href="">{props.link3}</a>
                <a href="">{props.link2}</a>
                <a href="">{props.link1}</a>
            </nav>
        </div>
    );

};
// const Mainpic = (props) => {
//     return (
//         <div className="main-pic">
//             <h1>{props.title}</h1>
//         </div>
//     );
// };
// const Content = (props) => {
//     return (
//         <div className="item">
//             <h3>{props.title}</h3>
//             <img src={props.img}/>
//             <p>{props.text}</p>
//             <button>{props.btn}</button>
//         </div>
//     );
// };
// const Sidebar = (props) => {
//     return (
//         <div className="sidebar">
//             <a href="">{props.text1}</a>
//             <a href="">{props.text2}</a>
//             <a href="">{props.text3}</a>
//             <a href="">{props.text4}</a>
//             <a href="">{props.text5}</a>
//         </div>
//     );
// };
// const Footer = (props) => {
//     return (
//         <footer className="footer">
//             <p>{props.text}</p>
//         </footer>
//     );
// };

export default Header;