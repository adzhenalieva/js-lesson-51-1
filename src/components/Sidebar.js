import React from "react";

const Sidebar = (props) => {
    return (
        <div className="sidebar">
            <a href="">{props.text1}</a>
            <a href="">{props.text2}</a>
            <a href="">{props.text3}</a>
            <a href="">{props.text4}</a>
            <a href="">{props.text5}</a>
        </div>
    );
};
export default Sidebar;