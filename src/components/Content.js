import React from "react";

const Content = (props) => {
    return (
        <div className="item">
            <h3>{props.title}</h3>
            <img src={props.img}/>
            <p>{props.text}</p>
            <button>{props.btn}</button>
        </div>
    );
};
    export default Content;