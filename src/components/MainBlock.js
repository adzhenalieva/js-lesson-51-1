import React from "react";
const Mainpic = (props) => {
    return (
        <div className="main-pic">
            <h1>{props.title}</h1>
        </div>
    );
};
export default Mainpic;